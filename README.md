# Blog Challenge 
Se trata de un sitio web dinámico que permite administrar post de un blog que cuenta con diseño responsive y manejo de errores.

### Tecnologías 
* .NET Framework 4.5.2 y C# 6.0
* SQL Server 2014 

### Pasos para probar página web  
* Hay que crear la base de datos "blog", ejecutando el archivo Blog.sql 
* Cambiar el connectionStrings en data source = "nombre del servidor", initial catalog = "nombre de la base de datos", user id = "nombre de usuario de base de datos", password = "password de base de datos"
* Por último queda ejecutar el proyecto


