﻿using BlogChallenge.Dao;
using BlogChallenge.Model;
using BlogChallenge.Service;
using BlogChallenge.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogChallenge.Controllers
{
    public class PostController : Controller
    {
        BlogEntities context = new BlogEntities();
        PostDao postDao = new PostDao();
        PostService postService = new PostService();
        

        public List<Post> getAll(){
            return postDao.getAllPostDao();
        }

        public Post getById(int? id = null)
        {
            return postDao.GetPostIdDao(id);
        }

        [HttpPost]
        public ActionResult createPost(string title, string category, string content, HttpPostedFileBase file)
        {
            PostValidateModel pvModel = postService.validatePostService(title,category,content,file);
            if (pvModel.countError == 0)
            {
                this.create(title, category, content, file);
                return RedirectToAction("Index", "Blog");
            }
            else
            {
                TempData["pvModel"] = pvModel; 
                return RedirectToAction("creationFormPost", "Blog");
            }
        }

        public void create(string title, string category, string content, HttpPostedFileBase file)
        {
            DateTime now = DateTime.Now;
            string image = (now.ToString("yyyyMMddHHmmss") + "-" + file.FileName).ToLower();
            file.SaveAs(Server.MapPath("~/image/" + image));
            this.postDao.createNewPostDao(title, content, category, image, now);
        }

        public ActionResult delete(int? id = null)
        {
            Post post = this.getById(id);
            if (post == null)
            {
                return RedirectToAction("postNotFound", "Error");
            }
            /*string image = Path.Combine(HttpContext.Server.MapPath("/image/"), post.ImagePost);
            System.IO.File.Delete(image);*/
            postDao.softDeleteDao(id);
            return RedirectToAction("Index", "Blog");
        }  

        [HttpGet]
        public ActionResult detail(int? id = null)
        {
            Post post = this.getById(id);
            if (post == null)
            {
                return RedirectToAction("postNotFound", "Error");
            }
            return View(post);
        }

        [HttpGet]
        public ActionResult updateFormPost(int? id = null)
        {
            Post post = this.getById(id);
            PostValidateModel pvModel = TempData["pvModel"] as PostValidateModel;
            UpdateFormPostModel ufpModel = new UpdateFormPostModel();
            ufpModel.pvModel = pvModel;
            ufpModel.post = post;
            if (post == null)
            {
                return RedirectToAction("postNotFound", "Error");
            }
            return View(ufpModel);
        }

        [HttpPost]
        public ActionResult validateAndUpdatePost(int id,string title,string content,string category, HttpPostedFileBase file)
        {
            PostValidateModel pvModel = this.postService.validateUpdatePostService(title, content, file);
            if (pvModel.countError == 0) {
                this.update(id, title, content, category, file);
                return RedirectToAction("Index", "Blog");
            }
            TempData["pvModel"] = pvModel;
            return RedirectToAction("updateFormPost", "Post", new { id = id });
        }

        public void update(int id, string title, string content, string category, HttpPostedFileBase file)
        {
            if (file != null)
            {
                Post post = this.getById(id);
                string image = Path.Combine(HttpContext.Server.MapPath("/image/"), post.ImagePost);
                System.IO.File.Delete(image);
                string imagePost = (DateTime.Now.ToString("yyyyMMddHHmmss") + "-" + file.FileName).ToLower();
                file.SaveAs(Server.MapPath("~/image/" + imagePost));
                this.postDao.updatePostDao(id, title, content, category, imagePost);
            }
            else
            {
                this.postDao.updatePostDao(id, title, content, category);
            }
        }

    }
}