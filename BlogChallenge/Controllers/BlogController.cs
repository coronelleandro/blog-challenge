﻿using BlogChallenge.Model;
using BlogChallenge.Service;
using BlogChallenge.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogChallenge.Controllers
{
    public class BlogController : Controller
    {
        // GET: Blog
        PostController postController = new PostController();
        BlogIndexModel blogIndexModel = new BlogIndexModel();
        PostService postService = new PostService();

        public ActionResult index()
        {
            blogIndexModel.allPost = this.postController.getAll();
            return View(blogIndexModel);
        }

        public ActionResult creationFormPost()
        {
            PostValidateModel pvModel = TempData["pvModel"] as PostValidateModel;
            CreationFormPostModel creationFormPostModel = this.postService.newCreationFormPostModel(pvModel);
            return View(creationFormPostModel);
        }

        
    }
}