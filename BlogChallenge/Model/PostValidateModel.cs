﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogChallenge.Model
{
    public class PostValidateModel
    {
        public string titleError { set; get; }

        public string contentError { set; get; } 

        public string imageError { set; get; }

        public int countError { set; get; }

        public string title { set; get; }

        public string category { set; get; }

        public string content { set; get; }
    }
}