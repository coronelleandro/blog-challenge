﻿using BlogChallenge.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogChallenge.ViewModels
{
    public class CreationPostModel
    {
        public Post post { set; get; } 

        public PostValidateModel pvModel { set; get; }
    }
}