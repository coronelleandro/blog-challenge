﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BlogChallenge.Dao
{
    public class PostDao
    {
        BlogEntities context = new BlogEntities();

        public void createNewPostDao(string title,string content,string category,string image,DateTime now) {
            Post post = new Post();
            post.Title = title;
            post.Content = content;
            post.Category = category;
            post.ImagePost = image;
            post.CreationDate = now;
            post.isDeleted = false;
            this.context.Posts.Add(post);
            this.context.SaveChanges();
        }

        public Post GetPostIdDao(int? id = null)
        {
            return context.Posts.Where(p => p.ID == id)
                   .Where(p => p.isDeleted == false)   
                   .FirstOrDefault();
        }

        public List<Post> getAllPostDao()
        {
            return context.Posts.
                   Where(p => p.isDeleted == false). 
                   OrderByDescending(p => p.CreationDate).ToList();
        }

        public void deletePostDao(int? id)
        {
            Post post = context.Posts.Find(id);
            context.Posts.Remove(post);
            context.SaveChanges();
        }

        public void softDeleteDao(int? id)
        {
            Post post = context.Posts.Find(id);
            post.isDeleted = true;
            context.Posts.Attach(post);
            context.Entry(post).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void updatePostDao(int id,string title,string content,string category,string image = null)
        {
            Post post = context.Posts.Find(id);
            post.Title = title;
            post.Content = content;
            post.Category = category;
            if (image != null) {
                post.ImagePost = image;
            }
            context.Posts.Add(post);
            context.Posts.Attach(post);
            context.Entry(post).State = EntityState.Modified;
            context.SaveChanges();
        }
    }
}