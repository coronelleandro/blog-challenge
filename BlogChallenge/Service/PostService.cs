﻿using BlogChallenge.Dao;
using BlogChallenge.Model;
using BlogChallenge.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogChallenge.Service
{
    public class PostService
    {
        PostDao postDao = new PostDao();

        public PostValidateModel validatePostService(string title,string category,string content, HttpPostedFileBase file)
        {
            PostValidateModel pvModel = new PostValidateModel();
            pvModel.countError = 0;
            if (title == "")
            {
                pvModel.titleError = "El titulo es obligatorio";
                pvModel.countError = 1;
            }
            else if (title.Length <= 4)
            {
                pvModel.titleError = "El titulo no puede tener menos de 5 letras";
                pvModel.countError = 1;
            }

            if (content == "")
            {
                pvModel.contentError = "El contenido es obligatorio";
                pvModel.countError = 1;
            }
            else if (content.Length <= 4)
            {
                pvModel.contentError = "El contenido no puede tener menos de 5 letras";
                pvModel.countError = 1;
            }

            if (file == null)
            {
                pvModel.imageError = "La imagen es obligatoria";
                pvModel.countError = 1;
            }

            pvModel.title = title;
            pvModel.category = category;
            pvModel.content = content;
            return pvModel;
        }

        public PostValidateModel validateUpdatePostService(string title, string content, HttpPostedFileBase file)
        {
            PostValidateModel pvModel = new PostValidateModel();
            pvModel.countError = 0;
            if (title == "")
            {
                pvModel.titleError = "El titulo es obligatorio";
                pvModel.countError = 1;
            }
            else if (title.Length <= 4)
            {
                pvModel.titleError = "El titulo no puede tener menos de 5 letras";
                pvModel.countError = 1;
            }

            if (content == "")
            {
                pvModel.contentError = "El contenido es obligatorio";
                pvModel.countError = 1;
            }
            else if (content.Length <= 4)
            {
                pvModel.contentError = "El contenido no puede tener menos de 5 letras";
                pvModel.countError = 1;
            }

            return pvModel;
        }

        public CreationFormPostModel newCreationFormPostModel(PostValidateModel postValidateModel = null)
        {
            CreationFormPostModel cfpModel = new CreationFormPostModel();
            if (postValidateModel == null) {
                PostValidateModel pvModel = new PostValidateModel();
                cfpModel.pvModel = pvModel;
            }
            else
            {
                cfpModel.pvModel = postValidateModel;
            }
            return cfpModel;
        }
    }
}