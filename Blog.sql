create database Blog;
go 

use Blog;
go

create table Post(
	ID int identity primary key,
	Title varchar(150) not null,
	Content varchar(max) not null, 
	ImagePost varchar(max) not null,
	Category varchar(150) not null,
	CreationDate datetime not null,
	isDeleted bit not null
);
go

/*drop table Post;
go

select * from Post;
*/ 